package com.healthy.topUp.Repository;

import com.healthy.topUp.model.domain.Order;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {

    // Optional<Order> findById(Long id);

//    @Modifying
//    @Transactional
//    @Query("UPDATE DeliveryEntity SET status = ?1 WHERE id = ?2 ")
//    void setDeliveryStatusById(String status, UUID id);

//    @Modifying
//    @Transactional
//    @Query("UPDATE DeliveryEntity SET prices = ?1 WHERE id = ?2 ")
//    void updatePriceById(Order prices, UUID id);

    @Query("select order from Order as order")
    List<Order> getByPagination(Pageable pageable);

}