package com.healthy.topUp.Repository;

import com.healthy.topUp.model.domain.Menu;
import com.healthy.topUp.model.domain.Order;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MenuRepository extends JpaRepository<Menu, Long> {

    // Optional<Order> findById(Long id);

//    @Modifying
//    @Transactional
//    @Query("UPDATE DeliveryEntity SET status = ?1 WHERE id = ?2 ")
//    void setDeliveryStatusById(String status, UUID id);

//    @Modifying
//    @Transactional
//    @Query("UPDATE DeliveryEntity SET prices = ?1 WHERE id = ?2 ")
//    void updatePriceById(Order prices, UUID id);

    @Query("select menu from Menu as menu")
    List<Menu> getByPagination(Pageable pageable);

}