package com.healthy.topUp.Controller;

import com.healthy.topUp.exception.MenuNotExistException;
import com.healthy.topUp.model.ResponseWrapper;
import com.healthy.topUp.model.dto.MenuDTO;
import com.healthy.topUp.model.dto.PaginationDTO;
import com.healthy.topUp.service.MenuServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/menu")

public class MenuController {

    @Autowired
    private MenuServiceImpl orderService;

    @GetMapping("/getMenuById/{id}")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity getMenuById(@PathVariable Long id) {
        try {
            return ResponseEntity.
                    status(HttpStatus.OK)
                    .body(ResponseWrapper.builder().data(orderService.getMenuByID(id)).message("Menu fetched successfully").build());
        } catch (MenuNotExistException ex) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ResponseWrapper.builder().message(ex.getMessage()).build());
        }
    }


    @PostMapping("/createMenu")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity createNewMenu(@RequestBody MenuDTO menuDTO) {

        return ResponseEntity.
                status(HttpStatus.CREATED)
                .body(ResponseWrapper.builder().data(orderService.createMenuId(menuDTO)).message("Menu generated successfully").build());
    }

    @DeleteMapping("/deleteMenu")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public ResponseEntity deleteOrder(@RequestBody MenuDTO menuDTO) {
        try {
            return ResponseEntity.
                    status(HttpStatus.ACCEPTED)
                    .body(ResponseWrapper.builder().data(orderService.deleteMenu(menuDTO)).message("Menu deleted successfully").build());
        } catch (MenuNotExistException ex) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ResponseWrapper.builder().message(ex.getMessage()).build());
        }
    }

    @GetMapping("/getMenuByPages")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public ResponseEntity getAllOrdersByPages(@RequestParam("page") int page, @RequestParam("size") int size, @RequestParam("sorting") String sorting) {
        return ResponseEntity.
                status(HttpStatus.OK)
                .body(ResponseWrapper.builder().data(orderService.getAllByPagination(PaginationDTO.builder().page(page).size(size).sorting(sorting).build()))
                        .message("Menu list fetched successfully").build());
    }


    @GetMapping("/getMenuByPagination")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public ResponseEntity getAllOrdersByPagination(@RequestParam("page") int page, @RequestParam("size") int size) {
        return ResponseEntity.
                status(HttpStatus.OK)
                .body(ResponseWrapper.builder().data(orderService.getByPagination(PaginationDTO.builder().page(page).size(size).build()))
                        .message("Orders list fetched successfully").build());
    }

}