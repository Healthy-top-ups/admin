package com.healthy.topUp.Controller;

import com.healthy.topUp.model.ResponseWrapper;
import com.healthy.topUp.model.dto.JwtResponse;
import com.healthy.topUp.model.dto.UserDTO;
import com.healthy.topUp.security.JwtUtils;
import com.healthy.topUp.service.UserDetailsImpl;
import com.healthy.topUp.service.UserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/auth")
public class AuthController {
    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserDetailsServiceImpl userService;

    @Autowired
    JwtUtils jwtUtils;

    @PostMapping("/signin")
    public ResponseEntity authenticateUser(@Valid @RequestBody UserDTO loginRequest) {

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);

        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        List<String> roles = new ArrayList<>();

        return ResponseEntity.ok(new JwtResponse(jwt,
                userDetails.getId(),
                userDetails.getUsername(),
                userDetails.getEmail(),
                roles));
    }

    @PostMapping("/signup")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity registerUser(@Valid @RequestBody UserDTO signUpRequest) {

        if (Boolean.TRUE.equals(userService.verifyUserName(signUpRequest.getUsername()))) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ResponseWrapper.builder().message("Error: userName is already in use!").build());
        }

        if (Boolean.TRUE.equals(userService.verifyUserEmailId(signUpRequest.getEmail()))) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ResponseWrapper.builder().message("Error: email is already in use!").build());
        }
        userService.saveUser(signUpRequest);
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(ResponseWrapper.builder().message("User registered successfully!").build());
    }
}
