package com.healthy.topUp.Controller;

import com.healthy.topUp.exception.UserNotExistException;
import com.healthy.topUp.model.ResponseWrapper;
import com.healthy.topUp.model.domain.User;
import com.healthy.topUp.model.dto.UserDTO;
import com.healthy.topUp.service.UserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserDetailsServiceImpl userService;

    @GetMapping("/getUser/{userName}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public ResponseEntity signUp(@PathVariable String userName) {
        return ResponseEntity.
                status(HttpStatus.CREATED)
                .body(ResponseWrapper.builder().data(userService.getUserByUsername(userName)).message("user profile fetched successfully").build());
    }

    @PutMapping("/updateUser")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public ResponseEntity updateUser(@RequestBody UserDTO userDTO) {
        return ResponseEntity.
                status(HttpStatus.CREATED)
                .body(ResponseWrapper.builder().data(userService.updateUserByUsername(userDTO)).message("user profile updated successfully").build());
    }

    @DeleteMapping("/deleteUser")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public ResponseEntity deleteUser(@RequestBody User user) {
        try {
            return ResponseEntity.
                    status(HttpStatus.CREATED)
                    .body(ResponseWrapper.builder().data(userService.deleteUser(user)).message("user profile Deleted successfully").build());
        } catch (UserNotExistException ex) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ResponseWrapper.builder().message(ex.getMessage()).build());
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ResponseWrapper.builder().message("first delete or change userName from customer_order table").build());
        }
    }

}
