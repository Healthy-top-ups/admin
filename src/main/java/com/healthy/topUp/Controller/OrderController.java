package com.healthy.topUp.Controller;

import com.healthy.topUp.exception.OrderNotExistException;
import com.healthy.topUp.model.ResponseWrapper;
import com.healthy.topUp.model.dto.OrderDTO;
import com.healthy.topUp.model.dto.PaginationDTO;
import com.healthy.topUp.service.OrderServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/order")

public class OrderController {

    @Autowired
    private OrderServiceImpl orderService;

    @GetMapping("/getOrderById/{id}")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity getOrderById(@PathVariable Long id) {
        try {
            return ResponseEntity.
                    status(HttpStatus.OK)
                    .body(ResponseWrapper.builder().data(orderService.getOrderByID(id)).message("Order fetched successfully").build());
        } catch (OrderNotExistException ex) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ResponseWrapper.builder().message(ex.getMessage()).build());
        }
    }

    @GetMapping("/getOrders")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public ResponseEntity getAllOrders() {
        return ResponseEntity.
                status(HttpStatus.OK)
                .body(ResponseWrapper.builder().data(orderService.getAllOrders()).message("Orders list fetched successfully").build());
    }

    @GetMapping("/getOrdersByPages")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public ResponseEntity getAllOrdersByPages(@RequestParam("page") int page, @RequestParam("size") int size, @RequestParam("sorting") String sorting) {
        return ResponseEntity.
                status(HttpStatus.OK)
                .body(ResponseWrapper.builder().data(orderService.getAllByPagination(PaginationDTO.builder().page(page).size(size).sorting(sorting).build()))
                        .message("Orders list fetched successfully").build());
    }

    @GetMapping("/getOrderByPagination")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public ResponseEntity getAllOrdersByPagination(@RequestParam("page") int page, @RequestParam("size") int size) {
        return ResponseEntity.
                status(HttpStatus.OK)
                .body(ResponseWrapper.builder().data(orderService.getByPagination(PaginationDTO.builder().page(page).size(size).build()))
                        .message("Orders list fetched successfully").build());
    }


    @PostMapping("/createOrder")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity createNewOrder(@RequestHeader(name = "userName", required = true) String userName) {
        return ResponseEntity.
                status(HttpStatus.CREATED)
                .body(ResponseWrapper.builder().data(orderService.createOrderId(userName)).message("Order generated successfully").build());
    }

    @PutMapping("/updateOrder")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public ResponseEntity updateOrder(@RequestBody OrderDTO orderDTO) {
        try {
            return ResponseEntity.
                    status(HttpStatus.CREATED)
                    .body(ResponseWrapper.builder().data(orderService.updateOrder(orderDTO)).message("Order updated successfully").build());
        } catch (OrderNotExistException ex) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ResponseWrapper.builder().message(ex.getMessage()).build());
        }
    }

    @DeleteMapping("/deleteOrder")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public ResponseEntity deleteOrder(@RequestBody OrderDTO orderDTO) {
        try {
            return ResponseEntity.
                    status(HttpStatus.ACCEPTED)
                    .body(ResponseWrapper.builder().data(orderService.deleteOrder(orderDTO)).message("Order deleted successfully").build());
        } catch (OrderNotExistException ex) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ResponseWrapper.builder().message(ex.getMessage()).build());
        }
    }
}