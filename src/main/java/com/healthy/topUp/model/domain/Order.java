package com.healthy.topUp.model.domain;

import com.healthy.topUp.configuration.BsonType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import java.time.ZonedDateTime;
import java.util.ArrayList;


@Data
@Entity
@Table(name = "customer_order")
@Builder
@NoArgsConstructor
@AllArgsConstructor
@TypeDef(name = "BsonType", typeClass = BsonType.class)
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "products")
    @Type(type = BsonType.BSON)
    private ArrayList<Products> products;
    @Column(name = "customer_name")
    private String customerName;
    @Column(name = "contact_info")
    private String contactInfo;
    @Column(name = "email")
    private String email;
    @Column(name = "order_type")
    private String orderType;
    @Column(name = "address")
    private String address;
    @Column(name = "payment_mode")
    private String paymentMode;
    @Column(name = "user_name")
    private String userName;

    @Column(name = "created")
    private ZonedDateTime created;

    @Column(name = "updated")
    private ZonedDateTime updated;

    public void setLastUpdate() {
        this.updated = ZonedDateTime.now();
    }
}