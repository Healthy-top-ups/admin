package com.healthy.topUp.model.domain;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Products {
    private String itemName;
    private String size;
    private String unitPrice;
    private Long quantity;
}
