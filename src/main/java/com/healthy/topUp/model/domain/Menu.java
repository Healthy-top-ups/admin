package com.healthy.topUp.model.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.ZonedDateTime;


@Data
@Entity
@Table(name = "menu")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Menu {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "recipe_name")
    private String recipeName;

    @Column(name = "quantity")
    private String quantity;

    @Column(name = "ingredient_name")
    private String ingredientName;

    @Column(name = "food_category")
    private String foodCategory;

    @Column(name = "preferred_meal_timing")
    private String preferredMealTiming;

    @Column(name = "user_name")
    private String userName;

    @Column(name = "description")
    private String description;

    @Column(name = "created")
    private ZonedDateTime created;

    @Column(name = "updated")
    private ZonedDateTime updated;

    public void setLastUpdate() {
        this.updated = ZonedDateTime.now();
    }

}