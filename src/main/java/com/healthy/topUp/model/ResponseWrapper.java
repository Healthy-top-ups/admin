package com.healthy.topUp.model;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class ResponseWrapper {

    private Object data;
    private String message;

}
