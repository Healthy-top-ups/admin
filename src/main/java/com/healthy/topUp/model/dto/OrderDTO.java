package com.healthy.topUp.model.dto;

import com.healthy.topUp.model.domain.Products;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrderDTO {
    private Long id;
    private ArrayList<Products> products;
    private String customerName;
    private String contactInfo;
    private String email;
    private String orderType;
    private String address;
    private String paymentMode;
    private String userName;
}