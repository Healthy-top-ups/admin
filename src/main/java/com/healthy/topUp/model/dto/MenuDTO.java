package com.healthy.topUp.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MenuDTO {
    private Long id;
    private String recipeName;
    private String quantity;
    private String ingredientName;
    private String foodCategory;
    private String preferredMealTiming;
    private String description;
    private String userName;
}
