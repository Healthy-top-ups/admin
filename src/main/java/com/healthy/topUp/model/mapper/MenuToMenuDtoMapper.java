package com.healthy.topUp.model.mapper;

import com.healthy.topUp.model.domain.Menu;
import com.healthy.topUp.model.dto.MenuDTO;
import org.mapstruct.Mapper;
import org.mapstruct.NullValueCheckStrategy;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface MenuToMenuDtoMapper {

    public final static MenuToMenuDtoMapper INSTANCE = Mappers.getMapper(MenuToMenuDtoMapper.class);

    MenuDTO menuToMenuDtoMapper(Menu menu);
}
