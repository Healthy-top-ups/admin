//package com.falabella.boss.order.payment.infrastructure.secondary.publisher.mapper;
//
//import com.falabella.boss.order.payment.core.domain.orderPaymentDocumentRequest.*;
//import com.falabella.boss.order.payment.core.domain.orderPaymentDocumentRequest.enums.TotalType;
//import com.falabella.boss.order.payment.infrastructure.secondary.publisher.dto.paymentDocumentGenerateRequest.*;
//import org.mapstruct.Mapper;
//import org.mapstruct.Mapping;
//import org.mapstruct.Named;
//import org.mapstruct.NullValueCheckStrategy;
//import org.springframework.util.CollectionUtils;
//
//import java.math.BigDecimal;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Optional;
//import java.util.stream.Collectors;
//
//@Mapper(componentModel = "spring", nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
//public interface OrderToPaymentDocumentGenerationRequestMapper {
//
//    @Mapping(target = "method", source = "paymentMethod")
//    @Mapping(target = "paymentMethodAmount", source = "amount")
//    PayedWithPubDTO paymentLineInfoToPaymentLineInfoMapper(PaymentLineInfo paymentLineInfo);
//
//
//    @Mapping(target = "currency", source = "amount.currency")
//    @Mapping(target = "centAmount", source = "amount.centAmount")
//    @Mapping(target = "fraction", source = "amount.fraction")
//    TaxAmountPubDTO taxToTaxAmountPubDTOMapper(Tax tax);
//
//
//    CustomInfoPubDTO customInfoToCustomInfoPubDTOMapper(CustomInfo customInfo);
//
//    @Mapping(target = "parentProduct", source = "orderLineUpc")
//    @Mapping(target = "upc", source = "orderLine.upc")
//    @Mapping(target = "name", source = "orderLine.productName")
//    @Mapping(target = "quantity", source = "orderLine.quantity")
//    @Mapping(target = "amount", source = "orderLine.unitAmount")
//    @Mapping(target = "payedWith", source = "orderLine.payedWith")
//    @Mapping(target = "discountAmounts", source = "orderLine.discounts")
//    @Mapping(target = "taxAmounts", source = "orderLine.taxes")
//    @Mapping(target = "customInfo", expression = "java(mapCustomInfo(orderLine.getCustomInfo(),orderLine.getDistributionOrderNumber()))")
//    ProductPubDTO orderLinesToProductPubDTOMapper(OrderLine orderLine, String orderLineUpc);
//
//
//    @Mapping(target = "customer.email", source = "email")
//    @Mapping(target = "customer.name", source = "fullName")
//    @Mapping(target = "customer.identificationDocument", source = "identificationDocument")
//    @Mapping(target = "address.municipalityName", source = "address.municipalName")
//    @Mapping(target = "address.country", source = "address.countryName")
//    BillingToPubDTO customerToBillingMapper(Customer customer);
//
//
//    AmountPubDTO itemTotalToAmountPubDTOMapper(Amount amount);
//
//
//    @Mapping(target = "externalOrderId", source = "orderId")
//    @Mapping(target = "storeId", ignore = true)
//    @Mapping(target = "subtotalAmount", source = "totals", qualifiedByName = "getSubtotalAmount")
//    @Mapping(target = "discountAmount", source = "totals", qualifiedByName = "getDiscountAmount")
//    @Mapping(target = "totalAmount", source = "totals", qualifiedByName = "getTotalAmount")
//    @Mapping(target = "billingTo", source = "customer", qualifiedByName = "getBillingTo")
//    @Mapping(target = "products", expression = "java(getProducts(orderPaymentDocumentRequest.getOrderLines(),orderPaymentDocumentRequest.getTotals()))")
//        //Call from publisher
//    PaymentDocumentGenerationRequestPubDTO map(OrderPaymentDocumentRequest orderPaymentDocumentRequest);
//
//    @Named("getSubtotalAmount")
//    default AmountPubDTO getSubtotalAmount(List<Total> totals) {
//
//        Amount itemTotal = totals.stream().filter(total -> total.getType()
//                .equals(TotalType.ITEM_TOTAL))
//                .map(Total::getAmount).findFirst().orElse(null);
//
//        Amount shippingTotal = totals.stream().filter(total -> total.getType()
//                .equals(TotalType.SHIPPING_TOTAL))
//                .map(Total::getAmount).findFirst().orElse(null);
//
//        AmountPubDTO itemTotalPub = this.itemTotalToAmountPubDTOMapper(itemTotal);
//        AmountPubDTO shippingTotalPub = this.itemTotalToAmountPubDTOMapper(shippingTotal);
//
//        return getSubtotalAmountByAdding(itemTotalPub, shippingTotalPub);
//    }
//
//
//    @Named("getDiscountAmount")
//    default AmountPubDTO getDiscountAmount(List<Total> totals) {
//
//        Amount discountTotal = totals.stream().filter(total -> total.getType()
//                .equals(TotalType.DISCOUNT_TOTAL))
//                .map(Total::getAmount).findFirst().orElse(null);
//
//        return this.itemTotalToAmountPubDTOMapper(discountTotal);
//    }
//
//
//    @Named("getTotalAmount")
//    default AmountPubDTO getTotalAmount(List<Total> totals) {
//
//        Amount itemTotal = totals.stream().filter(total -> total.getType()
//                .equals(TotalType.ITEM_TOTAL))
//                .map(Total::getAmount).findFirst().orElse(null);
//
//        Amount shippingTotal = totals.stream().filter(total -> total.getType()
//                .equals(TotalType.SHIPPING_TOTAL))
//                .map(Total::getAmount).findFirst().orElse(null);
//
//        Amount discountTotal = totals.stream().filter(total -> total.getType()
//                .equals(TotalType.DISCOUNT_TOTAL))
//                .map(Total::getAmount).findFirst().orElse(null);
//
//
//        AmountPubDTO itemTotalPub = this.itemTotalToAmountPubDTOMapper(itemTotal);
//        AmountPubDTO shippingTotalPub = this.itemTotalToAmountPubDTOMapper(shippingTotal);
//        AmountPubDTO subtotalAmount = getSubtotalAmountByAdding(itemTotalPub, shippingTotalPub);
//
//        AmountPubDTO discountTotalPub = this.itemTotalToAmountPubDTOMapper(discountTotal);
//        return getTotalAmountBySubtracting(subtotalAmount, discountTotalPub);
//    }
//
//
//    @Named("getBillingTo")
//    default BillingToPubDTO getBillingTo(Customer customer) {
//        return this.customerToBillingMapper(customer);
//    }
//
//    @Named("getProducts")
//    default List<ProductPubDTO> getProducts(List<OrderLine> orderLines, List<Total> totals) {
//        List<ProductPubDTO> productPubDTO = new ArrayList<>();
//        if (!CollectionUtils.isEmpty(orderLines)) {
//            productPubDTO = orderLines.stream()
//                    .map(orderLine -> {
//                                String orderLineUpc = getParentUpcObject(orderLines, orderLine);
//                                return this.orderLinesToProductPubDTOMapper(orderLine, orderLineUpc);
//                            }
//                    ).collect(Collectors.toList());
//        }
//        if (!CollectionUtils.isEmpty(totals)) {
//            Amount shippingTotal = totals.stream().filter(total -> total.getType()
//                    .equals(TotalType.SHIPPING_TOTAL))
//                    .map(Total::getAmount).findFirst().orElse(null);
//            AmountPubDTO shippingTotalPub = this.itemTotalToAmountPubDTOMapper(shippingTotal);
//            productPubDTO.add(ProductPubDTO.builder().amount(shippingTotalPub).quantity(1L).name(TotalType.SHIPPING_TOTAL.name()).build());
//        }
//        return productPubDTO;
//    }
//
//    default String getParentUpcObject(List<OrderLine> orderLines, OrderLine orderLine) {
//        return orderLines.stream()
//                .filter(orderLineUpc -> orderLine.getParentLineNumber() != null)
//                .filter(parentUpc -> parentUpc.getLineNumber().equals(orderLine.getParentLineNumber()))
//                .findFirst().map(OrderLine::getUpc).orElse("");
//    }
//
//    @Named("mapCustomInfo")
//    default List<CustomInfoPubDTO> mapCustomInfo(List<CustomInfo> customInfo, String distributionOrderNumber) {
//        List<CustomInfoPubDTO> customInfoList = new ArrayList<>();
//        if (!CollectionUtils.isEmpty(customInfo)) {
//            customInfoList = customInfo.stream().map(this::customInfoToCustomInfoPubDTOMapper).collect(Collectors.toList());
//        }
//        List<String> values = new ArrayList<>();
//        values.add(distributionOrderNumber);
//        customInfoList.add(CustomInfoPubDTO.builder().id("distributionOrderNumber").name("distributionOrderNumber").type("String").values(values).build());
//
//        return customInfoList;
//    }
//
//
//    default AmountPubDTO getSubtotalAmountByAdding(AmountPubDTO itemTotalPub, AmountPubDTO shippingTotalPub) {
//
//        if (shippingTotalPub == null)
//            return itemTotalPub;
//
//        BigDecimal shippingTotal = Optional.of(shippingTotalPub).map(AmountPubDTO::calculateAmount).orElse(null);
//        BigDecimal itemTotal = Optional.ofNullable(itemTotalPub).map(AmountPubDTO::calculateAmount).orElse(null);
//        if (shippingTotal == null || itemTotal == null)
//            return shippingTotalPub;
//
//        BigDecimal subTotalAmountSum = shippingTotal.add(itemTotal);
//
//        Long fraction = (long) Math.pow(10, Math.max(0, subTotalAmountSum.stripTrailingZeros().scale()));
//        String centAmount = subTotalAmountSum.stripTrailingZeros().toPlainString().replace(".", "");
//        String currency = shippingTotalPub.getCurrency();
//        return AmountPubDTO.builder().centAmount(Long.valueOf(centAmount)).fraction(fraction).currency(currency).build();
//    }
//
//
//    default AmountPubDTO getTotalAmountBySubtracting(AmountPubDTO subtotalAmount, AmountPubDTO discountTotalPub) {
//
//        if (discountTotalPub == null)
//            return subtotalAmount;
//
//        BigDecimal subtotalAmountSum = Optional.ofNullable(subtotalAmount).map(AmountPubDTO::calculateAmount).orElse(null);
//        BigDecimal discountTotal = Optional.of(discountTotalPub).map(AmountPubDTO::calculateAmount).orElse(null);
//        if (subtotalAmountSum == null || discountTotal == null)
//            return discountTotalPub;
//
//        BigDecimal totalAmount = subtotalAmountSum.subtract(discountTotal);
//
//        Long fraction = (long) Math.pow(10, Math.max(0, totalAmount.stripTrailingZeros().scale()));
//        String centAmount = totalAmount.stripTrailingZeros().toPlainString().replace(".", "");
//        String currency = discountTotalPub.getCurrency();
//        return AmountPubDTO.builder().centAmount(Long.valueOf(centAmount)).fraction(fraction).currency(currency).build();
//    }
//
//}
