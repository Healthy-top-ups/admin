package com.healthy.topUp.model.mapper;


import com.healthy.topUp.model.domain.Menu;
import com.healthy.topUp.model.dto.MenuDTO;
import org.mapstruct.Mapper;
import org.mapstruct.NullValueCheckStrategy;

@Mapper(componentModel = "spring", nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface MenuDtoToMenuMapper {

    Menu menuDtoToMenuMapper(MenuDTO menuDTO);

}