package com.healthy.topUp.model.mapper;

import com.healthy.topUp.model.domain.Order;
import com.healthy.topUp.model.dto.OrderDTO;
import org.mapstruct.Mapper;
import org.mapstruct.NullValueCheckStrategy;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface OrderToOrderDtoMapper {

    public final static OrderToOrderDtoMapper INSTANCE = Mappers.getMapper(OrderToOrderDtoMapper.class);

    OrderDTO oderToOrderDtoMapper(Order order);
}
