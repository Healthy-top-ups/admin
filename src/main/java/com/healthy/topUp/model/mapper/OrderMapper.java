package com.healthy.topUp.model.mapper;


import com.healthy.topUp.model.domain.Order;
import com.healthy.topUp.model.dto.OrderDTO;
import org.mapstruct.Mapper;
import org.mapstruct.NullValueCheckStrategy;

@Mapper(componentModel = "spring", nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public
interface OrderMapper {

    OrderDTO oderDtoToOrderMapper(Order order);

}
