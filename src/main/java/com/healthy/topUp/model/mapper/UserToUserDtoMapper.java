package com.healthy.topUp.model.mapper;


import com.healthy.topUp.model.domain.User;
import com.healthy.topUp.model.dto.UserDTO;
import org.mapstruct.Mapper;
import org.mapstruct.NullValueCheckStrategy;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)

public interface UserToUserDtoMapper {

    public final static UserToUserDtoMapper INSTANCE = Mappers.getMapper(UserToUserDtoMapper.class);

    UserDTO userToUserDtoMapper(User user);
}
