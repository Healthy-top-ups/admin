package com.healthy.topUp.model.mapper;

import com.healthy.topUp.model.domain.Order;
import com.healthy.topUp.model.domain.User;
import com.healthy.topUp.model.dto.OrderDTO;
import com.healthy.topUp.model.dto.UserDTO;
import org.mapstruct.Mapper;
import org.mapstruct.NullValueCheckStrategy;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)

public interface UserDtoToUserMapper {

    public final static UserDtoToUserMapper INSTANCE = Mappers.getMapper(UserDtoToUserMapper.class);

    User userDtoToUserMapper(UserDTO userDTO);
}
