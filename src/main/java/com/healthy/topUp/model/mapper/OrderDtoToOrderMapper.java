package com.healthy.topUp.model.mapper;


import com.healthy.topUp.model.domain.Order;
import com.healthy.topUp.model.dto.OrderDTO;
import org.mapstruct.Mapper;
import org.mapstruct.NullValueCheckStrategy;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface OrderDtoToOrderMapper {

    public final static OrderDtoToOrderMapper INSTANCE = Mappers.getMapper(OrderDtoToOrderMapper.class);

    Order oderDtoToOrderMapper(OrderDTO orderDTO);

}