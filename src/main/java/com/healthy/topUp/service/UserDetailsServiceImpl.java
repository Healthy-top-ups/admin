package com.healthy.topUp.service;

import com.healthy.topUp.Repository.UserRepository;
import com.healthy.topUp.exception.UserNotExistException;
import com.healthy.topUp.model.domain.User;
import com.healthy.topUp.model.dto.UserDTO;
import com.healthy.topUp.model.mapper.UserDtoToUserMapper;
import com.healthy.topUp.model.mapper.UserToUserDtoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZonedDateTime;
import java.util.Optional;


@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    private UserDtoToUserMapper userDtoToUserMapper;

    @Autowired
    private UserToUserDtoMapper userToUserDtoMapper;

    @Autowired
    PasswordEncoder encoder;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException("User Not Found with username: " + username));
        return UserDetailsImpl.build(user);
    }

    @Transactional
    public UserDTO saveUser(UserDTO userDTO) {
        User user = userDtoToUserMapper.userDtoToUserMapper(userDTO);
        user.setPassword(encoder.encode(userDTO.getPassword()));
        user.setCreated(ZonedDateTime.now());
        user.setUpdated(ZonedDateTime.now());
        return userToUserDtoMapper.userToUserDtoMapper(userRepository.save(user));
    }

    @Transactional
    public UserDTO deleteUser(User userId) throws UserNotExistException {

        Optional<User> user = userRepository.findById(userId.getId());
        if (!user.isPresent()) {
            throw new UserNotExistException("User does not exist in database");
        }
        userRepository.deleteById(userId.getId());
        return userToUserDtoMapper.userToUserDtoMapper(user.get());
    }

    @Transactional
    public UserDTO getUserByUsername(String userName) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(userName)
                .orElseThrow(() -> new UsernameNotFoundException("User Not Found with username: " + userName));
        return userToUserDtoMapper.userToUserDtoMapper(user);
    }

    @Transactional
    public Boolean verifyUserEmailId(String emailId) throws UsernameNotFoundException {
        return userRepository.existsByEmail(emailId);
    }

    @Transactional
    public Boolean verifyUserName(String userName) throws UsernameNotFoundException {
        return userRepository.existsByUsername(userName);
    }


    @Transactional
    public UserDTO updateUserByUsername(UserDTO userDTO) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(userDTO.getUsername())
                .orElseThrow(() -> new UsernameNotFoundException("User Not Found with username: " + userDTO.getUsername()));
        if (userDTO.getId() == null) {
            userDTO.setId(user.getId());
        }
        User userEntity = userDtoToUserMapper.userDtoToUserMapper(userDTO);
        userEntity.setPassword(encoder.encode(userDTO.getPassword()));
        userEntity.setLastUpdate();
        userEntity.setCreated(user.getCreated());
        return userToUserDtoMapper.userToUserDtoMapper(userRepository.save(userEntity));
    }


}
