package com.healthy.topUp.service;

import com.healthy.topUp.Repository.OrderRepository;
import com.healthy.topUp.exception.OrderNotExistException;
import com.healthy.topUp.model.domain.Order;
import com.healthy.topUp.model.dto.OrderDTO;
import com.healthy.topUp.model.dto.PaginationDTO;
import com.healthy.topUp.model.mapper.OrderDtoToOrderMapper;
import com.healthy.topUp.model.mapper.OrderToOrderDtoMapper;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.JpaSort;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class OrderServiceImpl {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private EntityManager entityManager;

    private OrderDtoToOrderMapper orderDtoToOrderMapper = Mappers.getMapper(OrderDtoToOrderMapper.class);
    private OrderToOrderDtoMapper orderToOrderDtoMapper = Mappers.getMapper(OrderToOrderDtoMapper.class);

    public OrderDTO getOrderByID(Long id) throws OrderNotExistException {
        return orderRepository.findById(id).map(order -> orderToOrderDtoMapper.oderToOrderDtoMapper(order)).orElseThrow(() -> new OrderNotExistException("Order does not exist in database"));
    }

    public List<OrderDTO> getAllOrders() {
        return orderRepository.findAll().stream().map(order -> orderToOrderDtoMapper.oderToOrderDtoMapper(order)).collect(Collectors.toList());
    }

    public Long createOrderId(String userName) {
        return orderRepository.save(Order.builder().userName(userName).created(ZonedDateTime.now()).updated(ZonedDateTime.now()).build()).getId();
    }


    public OrderDTO updateOrder(OrderDTO orderDTO) throws OrderNotExistException {

        Optional<Order> order = orderRepository.findById(orderDTO.getId());
        if (!order.isPresent()) {
            throw new OrderNotExistException("Order does not exist in database");
        }
        Order orderEntity = orderDtoToOrderMapper.oderDtoToOrderMapper(orderDTO);
        orderEntity.setLastUpdate();
        orderEntity.setCreated(order.get().getCreated());
        return orderToOrderDtoMapper.oderToOrderDtoMapper(orderRepository.save(orderEntity));
    }

    public OrderDTO deleteOrder(OrderDTO orderDTO) throws OrderNotExistException {

        Optional<Order> order = orderRepository.findById(orderDTO.getId());
        if (!order.isPresent()) {
            throw new OrderNotExistException("Order does not exist in database");
        }
        orderRepository.deleteById(orderDTO.getId());
        return orderToOrderDtoMapper.oderToOrderDtoMapper(order.get());
    }

    public List<OrderDTO> getAllByPagination(PaginationDTO paginationDTO) {
        Pageable pageable = PageRequest.of(paginationDTO.getPage(), paginationDTO.getSize(), JpaSort.unsafe(Sort.Direction.ASC, paginationDTO.getSorting()));
        return orderRepository.getByPagination(pageable).stream().map(order -> orderToOrderDtoMapper.oderToOrderDtoMapper(order)).collect(Collectors.toList());
    }

    public List<OrderDTO> getByPagination(PaginationDTO paginationDTO) {
        List<Order> posts = entityManager.createQuery("select order from Order as order")
                .setFirstResult(paginationDTO.getPage())
                .setMaxResults(paginationDTO.getSize())
                .getResultList();
        return posts.stream().map(post -> orderToOrderDtoMapper.oderToOrderDtoMapper(post)).collect(Collectors.toList());
    }
}
