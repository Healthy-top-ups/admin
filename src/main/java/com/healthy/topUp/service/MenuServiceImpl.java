package com.healthy.topUp.service;

import com.healthy.topUp.Repository.MenuRepository;
import com.healthy.topUp.exception.MenuNotExistException;
import com.healthy.topUp.model.domain.Menu;
import com.healthy.topUp.model.dto.MenuDTO;
import com.healthy.topUp.model.dto.PaginationDTO;
import com.healthy.topUp.model.mapper.MenuDtoToMenuMapper;
import com.healthy.topUp.model.mapper.MenuToMenuDtoMapper;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.JpaSort;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class MenuServiceImpl {

    @Autowired
    private MenuRepository menuRepository;

    @Autowired
    EntityManager entityManager;

    private MenuDtoToMenuMapper menuDtoToMenuMapper = Mappers.getMapper(MenuDtoToMenuMapper.class);
    private MenuToMenuDtoMapper menuToMenuDtoMapper = Mappers.getMapper(MenuToMenuDtoMapper.class);

    public MenuDTO getMenuByID(Long id) throws MenuNotExistException {
        return menuRepository.findById(id).map(menu -> menuToMenuDtoMapper.menuToMenuDtoMapper(menu)).orElseThrow(() -> new MenuNotExistException("Menu does not exist in database"));
    }

    public MenuDTO createMenuId(MenuDTO menuDTO) {
        Menu menu = menuDtoToMenuMapper.menuDtoToMenuMapper(menuDTO);
        menu.setCreated(ZonedDateTime.now());
        menu.setUpdated(ZonedDateTime.now());
        return menuToMenuDtoMapper.menuToMenuDtoMapper(menuRepository.save(menu));
    }

    public MenuDTO deleteMenu(MenuDTO menuDTO) throws MenuNotExistException {
        Optional<Menu> menu = menuRepository.findById(menuDTO.getId());
        if (!menu.isPresent()) {
            throw new MenuNotExistException("Menu does not exist in database");
        }
        menuRepository.deleteById(menuDTO.getId());
        return menuToMenuDtoMapper.menuToMenuDtoMapper(menu.get());
    }

    public List<MenuDTO> getAllByPagination(PaginationDTO paginationDTO) {
        Pageable pageable = PageRequest.of(paginationDTO.getPage(), paginationDTO.getSize(), JpaSort.unsafe(Sort.Direction.ASC, paginationDTO.getSorting()));
        return menuRepository.getByPagination(pageable).stream().map(menu -> menuToMenuDtoMapper.menuToMenuDtoMapper(menu)).collect(Collectors.toList());
    }

    public List<MenuDTO> getByPagination(PaginationDTO paginationDTO) {
        List<Menu> posts = entityManager.createQuery("select menu from Menu as menu")
                .setFirstResult(paginationDTO.getPage())
                .setMaxResults(paginationDTO.getSize())
                .getResultList();
        return posts.stream().map(post -> menuToMenuDtoMapper.menuToMenuDtoMapper(post)).collect(Collectors.toList());
    }


}
