package com.healthy.topUp.exception;

public class OrderNotFinishedException extends Exception {

    public OrderNotFinishedException(String message) {
        super(message);
    }
}
