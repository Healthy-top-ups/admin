package com.healthy.topUp.exception;

public class OrderAlreadyDeliverException extends Exception {

    public OrderAlreadyDeliverException(String message) {
        super(message);
    }
}
