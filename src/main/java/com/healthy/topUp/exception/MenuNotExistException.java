package com.healthy.topUp.exception;

public class MenuNotExistException extends Exception {

    public MenuNotExistException(String message) {
        super(message);
    }
}
