package com.healthy.topUp.exception;

public class DishNotExistException extends Exception {

    public DishNotExistException(String message) {
        super(message);
    }
}
