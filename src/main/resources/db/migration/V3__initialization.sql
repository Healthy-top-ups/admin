CREATE TABLE IF NOT EXISTS menu
(
    id SERIAL PRIMARY KEY,
    recipe_name VARCHAR,
    quantity VARCHAR,
    ingredient_name  VARCHAR,
    food_category VARCHAR,
    preferred_meal_timing VARCHAR,
    created TIMESTAMP,
    updated TIMESTAMP,
    description VARCHAR,
    user_name VARCHAR NOT NULL
);