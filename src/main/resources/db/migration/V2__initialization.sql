CREATE TABLE IF NOT EXISTS user_profile
(
    id SERIAL PRIMARY KEY,
    user_name VARCHAR NOT NULL UNIQUE,
    user_first_name VARCHAR,
    user_last_name VARCHAR ,
    password  VARCHAR,
    phone_number VARCHAR,
    email  VARCHAR,
    city  VARCHAR,
    country  VARCHAR,
    postal_code VARCHAR,
    about_me VARCHAR,
    created TIMESTAMP,
    updated TIMESTAMP
);

CREATE TABLE IF NOT EXISTS customer_order
(
    id SERIAL PRIMARY KEY,
    products jsonb ,
    contact_info VARCHAR,
    customer_name VARCHAR,
    email  VARCHAR,
    order_type  VARCHAR,
    address  VARCHAR,
    payment_mode VARCHAR,
    created TIMESTAMP,
    updated TIMESTAMP,
    user_name VARCHAR NOT NULL,
    FOREIGN KEY (user_name) REFERENCES user_profile (user_name)
);